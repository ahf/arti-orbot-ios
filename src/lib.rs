use std::ffi::CStr;
use std::fs::File;
use std::thread;

use arti::{run, ArtiConfig};
use arti_client::TorClientConfig;
use std::os::raw::{c_char, c_int};
use tor_config::ConfigurationSources;
use tor_rtcompat::BlockOn;
use tor_rtcompat::PreferredRuntime;

#[no_mangle]
pub extern "C" fn arti_listen(socks_port: c_int, dns_port: c_int, log_file: *const c_char) {
    let runtime = PreferredRuntime::create().unwrap();
    let tor_client_config = TorClientConfig::default();
    let arti_config = ArtiConfig::default();
    let configuration_sources = ConfigurationSources::default();

    let rt_copy = runtime.clone();

    if !log_file.is_null() {
        let path = unsafe {
            let cstr = CStr::from_ptr(log_file);
            cstr.to_string_lossy().into_owned()
        };
        let file = File::options().append(true).open(path).unwrap();
        let (appender, _guard) = tracing_appender::non_blocking(file);
        tracing_subscriber::fmt().with_writer(appender).init();
    }

    thread::spawn(move || {
        rt_copy
            .block_on(run(
                runtime,
                socks_port as u16,
                dns_port as u16,
                configuration_sources,
                arti_config,
                tor_client_config,
            ))
            .unwrap();
    });
}
